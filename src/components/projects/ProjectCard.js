import React from 'react';
import gitlab from '../../assets/icons/gitlab.svg'

const ProjectCard = ({project: {name, image, deployed_url, gitlab_url}}) => {
    return (
        <div className="projectCard col-md-6 col-lg-4 my-2">
            <figure className="projectCard__wrapper">
                    <a href={deployed_url} target="_blank" rel="noopener noreferrer">
                        <img src={image} alt={name} className="projectCard__image"/>
                    </a>
                <div className="projectCard__title">
                    <a href={gitlab_url} target="_blank" rel="noopener noreferrer">
                        <img src={gitlab} alt="github link" className="projectCard__icon"/>
                    </a>
                    {name}
                </div>
            </figure>
        </div>
    );
};

export default ProjectCard;